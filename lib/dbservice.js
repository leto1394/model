'use strict'

const internals = {
  pools: {},
  driver: require('mysql2')
}

const injectConnectOptions = function (instance, sql) {
  instance.justStarted = false
  console.log('SET AUTOCOMMIT=0;')
  if (typeof sql === 'string') {
    sql = 'SET AUTOCOMMIT=0;' + sql
  } else {
    if (typeof sql.sql === 'string') {
      sql.sql = 'SET AUTOCOMMIT=0;' + sql.sql
    }
  }
}

class DBService {
  constructor (cfg, debug, options) {
    this.cfg = cfg
    this.justStarted = true
    options = options || {}
    this.cfg.connectionLimit = this.cfg.connectionLimit || 4
    this.db = this.cfg.database
    this.debug = debug
    if (!internals.pools[this.cfg.database]) {
      internals.pools[this.cfg.database] = internals.driver.createPool(this.cfg)
    }
    this.pools = internals.pools
    if (options.serviceConn) {
      let serviceCfg = Object.assign({}, this.cfg)
      delete serviceCfg.database
      let serviceConn = internals.driver.createConnection(serviceCfg)
      this.serviceConn = (sql) => {
        if (this.justStarted) {
          sql = injectConnectOptions(this, sql)
        }
        return new Promise((resolve, reject) => {
          serviceConn.query(sql, (err, result) => {
            err ? reject(err) : resolve(result)
          })
        })
      }
    }
    this.connCounter = 0
    return this
  }

  getPool () {
    return this.pools[this.db]
  }

  logTime (startTime, info) {
    this.debug && console.log(`${info} >>>>> execute in ${Date.now() - startTime}ms`)
  }

  getConn () {
    const me = this
    return new Promise(function (resolve, reject) {
      me.getPool().getConnection((err, conn) => {
        err ? reject(err, conn) : resolve(conn)
      })
    })
  }

  doConn (conn, opts) {
    const me = this
    const startTime = Date.now()
    let info = opts.values ? `DB.${this.cfg.database}.${opts.sql || opts} with values ${opts.values}` : `DB.${this.cfg.database}.${opts.sql || opts}`
    return new Promise(function (resolve, reject) {
      conn.query(opts, function (err, results, fields) {
        me.logTime(startTime, info)
        if (err) {
          conn && conn.release()
          return reject(err, conn)
        }
        conn.release()
        return resolve(results, fields, conn)
      })
    })
  }

  query (conn, opts) {
    return new Promise((resolve, reject) => {
      return conn.query(opts, (err, rows, fields) => {
        if (err) {
          return reject(err)
        }
        return resolve({rows, fields, conn})
      })
    })
  }

  async do (opts) {
    const me = this
    const startTime = Date.now()
    let info = opts.values ? `DB.${this.cfg.database}.${opts.sql || opts} with values ${opts.values}` : `DB.${this.cfg.database}.${opts.sql || opts}`
    let conn = await this.getConn()
    let result
    let {wrapBefore, wrapAfter, returnConnection, foundRows} = opts
    if (returnConnection) {
      me.logTime(startTime, info)
      return this.query(conn,opts)
    }
    if (foundRows && opts.sql.toLowerCase().trim().indexOf('select ') === 0) {
      let sql = opts.sql.trim().split(' ')
      let s = sql.shift()
      sql.unshift(`${s} SQL_CALC_FOUND_ROWS *,`)
      opts.sql = sql.join(' ')
      result = await this.query(conn, opts)
      result.count = await this.query(conn, 'SELECT FOUND_ROWS() AS count').then(({rows}) => rows[0].count)
      conn.release()
      me.logTime(startTime, info)
      return result
    }
    if (wrapBefore || wrapAfter) {
      if (wrapBefore) {
        result.wrapBefore = await this.query(conn, wrapBefore)
      }
      result.rows = await this.query(conn, opts)
      if (wrapAfter) {
        result.wrapAfter = await this.query(conn, wrapAfter)
      }
      conn.release()
      me.logTime(startTime, info)
      return result
    }
    result = await this.query(conn, opts).then(({rows}) => rows)
    conn.release()
    me.logTime(startTime, info)
    return result
  }

  doV1 (opts) {
    const me = this
    const startTime = Date.now()
    let info = opts.values ? `DB.${this.cfg.database}.${opts.sql || opts} with values ${opts.values}` : `DB.${this.cfg.database}.${opts.sql || opts}`
    return new Promise(function (resolve, reject) {
      me.getPool().getConnection((err, conn) => {
        if (err) {
          conn && conn.release()
          me.logTime(startTime, info)
          return reject(err)
        } else {
          conn.query(opts, function (err, results, fields) {
            if (err) {
              conn.release()
              return reject(err)
            }
            me.logTime(startTime, info)
            if (opts.requestAfter) {
              resolve({results, fields, resultsAfter: me.doConn(conn, opts.requestAfter)})
            }
            if (opts.returnConnection) {
              resolve({results, fields, conn})
            }
            conn.release()
            return resolve({results})
          })
        }
      })
    })
  }
}

module.exports = DBService
